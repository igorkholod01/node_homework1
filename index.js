const express = require('express');
const fs = require('fs');
const morgan = require('morgan');

function checkDir(dir) {
    fs.access(dir, err => {
        if (err) {
            fs.mkdir(dir, err => {
                if (err) {
                    throw err;
                }
            });
        }
    });
}

function checkFileName(filename) {
    return filename.match(/^[\w.\-\s]+\.(log|txt|json|yaml|xml|js)$/);
}

const app = express();
const accessLogStream = fs.createWriteStream('access.log', {flags: 'a'})

app.use(express.json());
app.use(morgan('tiny', {stream: accessLogStream}));

const FILE_FOLDER = 'files/';
checkDir('files');

app.post('/api/files', (req, res) => {
    const fileName = req.body.filename;
    const content = req.body.content;
    if (fileName && content) {
        if (checkFileName(fileName)) {
            checkDir('files');
            fs.writeFile(FILE_FOLDER + fileName, content, 'utf-8', err => {
                if (err) {
                    res.status(500).json({
                        'message': 'Server error'
                    });
                } else {
                    res.status(200).json({
                        'message': 'File created successfully!'
                    });
                }
            });
        } else {
            res.status(400).json({
                'message': 'Incorrect filename.'
            });
        }
    } else {
        let message;
        if (!fileName && !content) {
            message = 'Please, specify \'filename\' and \'content\' parameters!'
        } else {
            message = `Please, specify ${!fileName ? 'filename' : 'content'} parameter!`
        }
        res.status(400).json({
            'message': message
        });
    }
});

app.get('/api/files', (req, res) => {
    fs.readdir(FILE_FOLDER, (err, files) => {
        if (err) {
            res.status(500).json({
                'message': 'Internal server error'
            });
        } else {
            res.status(200).json({
                'message': 'Success',
                'files': files
            });
        }
    });
});

app.get('/api/files/:file', (req, res) => {
    const fileName = req.params.file;
    if (checkFileName(fileName)) {
        fs.readFile(FILE_FOLDER + fileName, 'utf-8', (err, content) => {
            if (err) {
                res.status(400).json({
                    'message': `No file with ${fileName} filename found`
                });
            } else {
                fs.stat(FILE_FOLDER + fileName, (err, stats) => {
                    res.status(200).json({
                        'message': 'Success',
                        'filename': fileName,
                        'content': content,
                        'extension': fileName.split('.').pop(),
                        'uploadedDate': stats.birthtime
                    });
                });
            }
        });
    } else {
        res.status(400).json({
            'message': 'Incorrect filename.'
        });
    }
})

app.post('/api/update', (req, res) => {
    const fileName = req.body.filename;
    const content = req.body.content;

    if (fileName && content) {
        if (checkFileName(fileName)) {
            checkDir('files');
            fs.appendFile(FILE_FOLDER + fileName, content, 'utf-8', err => {
                if (err) {
                    res.status(500).json({
                        'message': 'Server error'
                    });
                } else {
                    res.status(200).json({
                        'message': 'File updated successfully!'
                    });
                }
            });
        } else {
            res.status(400).json({
                'message': 'Incorrect filename.'
            })
        }
    } else {
        let message;
        if (!fileName && !content) {
            message = 'Please, specify \'filename\' and \'content\' parameters!'
        } else {
            message = `Please, specify ${!fileName ? 'filename' : 'content'} parameter!`
        }
        res.status(400).json({
            'message': message
        });
    }
});

app.delete('/api/delete/:file', (req, res) => {
    const fileName = req.params.file;

    if (checkFileName(fileName)) {
        fs.unlink(FILE_FOLDER + fileName, (err) => {
            if (err) {
                res.status(400).json({
                    'message': `No file with ${fileName} filename found`
                });
            } else {
                res.status(200).json({
                    'message': `File ${fileName} was successfully deleted`
                });
            }
        });
    } else {
        res.status(400).json({
            'message': 'Incorrect filename.'
        });
    }
});

app.listen(8080, () => {
    console.log('Server started at port 8080!');
});

